/*
 * randomstring.h
 *
 *  Created on: 2 maj 2014
 *      Author: hawker
 */

#include <ctime>
#include <cstdlib>
#include <string>
#include <cmath>
#ifndef RANDOMSTRING_H_
#define RANDOMSTRING_H_

static const char alphanum[] =
"0123456789"
"!@#$%^&*"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";
using namespace std;
int stringLength = sizeof(alphanum) - 1;

int gaussrand(double stddev, double mean)
{
	if(stddev<1) stddev=1;
	double x = 0;
	int i;
	for(i = 0; i < 100; i++)
		x += (double)rand() / RAND_MAX;

	x -= 100 / 2.0;
	x /= sqrt(100/ 12.0);

	return stddev*(int)(x+0.5)+mean;
}
int getGaussGene(int a, int b){
	double mean = (a+b)/2;
	if(abs(a-b)==1) mean=int(mean+0.5)+rand()%2;
 	double stddev = fabs(mean-a);
	return gaussrand(stddev,mean);
}
char genRandom()  // Random string generator function.
{
    return alphanum[rand() % stringLength];
}

/**
 * Zwraca losowy string
 * @param len dlugosc stringa
 * @param tmp z niego bedzie dostarczony lancuch do genomu 
 * @return losowy string
 */
char* genString(int len, char* tmp){
	for(int i = 0; i < len; ++i){
		tmp[i] = genRandom();
	}
	tmp[len] = '\0';
	
	return tmp;
}


#endif /* RANDOMSTRING_H_ */
