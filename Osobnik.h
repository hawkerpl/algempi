/*
 * Osobnik.h
 *
 *  Created on: 2 maj 2014
 *      Author: hawker
 */
#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <algorithm>
#include <string.h>

#include "randomstring.h"
#include <limits>
#ifndef OSOBNIK_H_
#define OSOBNIK_H_
using namespace std;
struct Osobnik {
	char* genome;
	int size;
	double fitness;
	bool evaulated;
};


/**
 * Nasza populacja czyli zwyk�y wektor Osobnik�w
 */
typedef vector<Osobnik> Populacja;

/**
 * Wektor podpopulacji, zawiera kilka mniejszych populajci
 */

typedef vector<Populacja> Podpopulacja;

/**
 * Wypisuje populacje
 */

void printPopulacja(Populacja& p){
	for(Populacja::const_iterator it =p.begin();it!= p.end();++it){
		std::cout << it->fitness << " " << it->genome << std::endl;
	}
}

void printPodpopulacja(Podpopulacja pd){
	int t=0;
	for(Podpopulacja::iterator it = pd.begin();it!= pd.end();++it){
			printPopulacja(*it);
			cout<<t<<endl;
			++t;
		}
}

/**
 * Inicjalizacja populacji
 * @param p wektor do inicjalizacji
 * @param len d�ugosc pojedynczego genomu
 * @return zainicjalizowana populacja
 */



Populacja initializePopulation(Populacja p, int len){
	for(unsigned int i = 0; i < p.size(); ++i){
		char tmp[len];
		p[i].size = len;
		p[i].genome = new char[p[i].size];
		strcpy(p[i].genome, genString(len, tmp));
		p[i].fitness = numeric_limits<double>::infinity();
	}

	return p;
}



/**
 * Dzieli populacje na tyle czesci ile jest procesow
 *
 * Przyk�ad
 * Populacja pop:
 *
 * AAAAAAAA
 * BBBBBBBB
 * CCCCCCCC
 * DDDDDDDD
 * EEEEEEEE
 *
 * Liczba procesow: 2
 *
 * zwraca:
 *
 * Podpopulacja result=dividePopulation(pop,2)
 * result[0]: obiekt typu Populacja zawierajacy osobniki:
 * AAAAAAAA
 * CCCCCCCC
 * EEEEEEEE
 *
 * result[1]: obiekt typu Populacja zawierajacy osobniki:
 * BBBBBBBB
 * DDDDDDDD
 *
 *
 *
 * @param p populacja do podzialu
 * @param num liczba procesow
 * @return obiekt podpopulacja, wektor populacji z miare przydzielonymi populacjami
 */
Podpopulacja dividePopulation(Populacja p, int num){
	int indPerProces = floor(p.size()/num);
	Podpopulacja v(num);
	for(unsigned int i=0;i<p.size();++i){
		v[i%num].push_back(p[i]);
	}

	return v;
}

/**
 * Funkcja dostosowania, liczy jak bardzo dany osobnik(string) jest podobny do stringu do ktorego dazymy
 * @param s osobnik
 * @param target cel
 * @return roznica pomiedzy dwoma, szukamy osobnika o najmniejszej roznicy
 */
double fitness(char* s, string target, int size){
	int sum = 0;
	for(unsigned int i = 0; i < size; ++i){
		sum += (int)(abs((int)s[i] - (int)target[i]));
	}
	
	return sum;
}
	

bool cmpOsobnik(Osobnik a,Osobnik b){
	return a.fitness<b.fitness;
}


Populacja 	sortPopulation(Populacja p){
	sort(p.begin(),p.end(),cmpOsobnik);
	return p;
}

Populacja normalize(Populacja p){
	int worstFitness = p[p.size()-1].fitness;
	int uplimit = 2*worstFitness;
	Populacja pcopy = p;
	int sum = 0, random;
	for(unsigned int i = 0; i < p.size(); ++i){
		int oldsum = sum;
		pcopy[i].fitness = uplimit-(p[i].fitness);
		sum += pcopy[i].fitness;
		pcopy[i].fitness = uplimit-(p[i].fitness)+oldsum;
	}

	return pcopy;
}

Populacja rulette(Populacja p){
	Populacja result;
	int worst = (int)p[p.size()-1].fitness;
	for(unsigned int i = 0; i < p.size(); ++i) {
		int random = rand()%worst;
		for(unsigned int j = 0; j < p.size(); ++j){
			if(random >= (j == 0 ? 0 : p[j-1].fitness) && random < p[j].fitness){
				result.push_back(p[j]);
				break;
			}
		}
	}

	return result;
}


Populacja crossover(Populacja p){
	int r=0;
	int size=p[0].size;
	int tabsize;
	Populacja result;
	if(p.size()%2==0){
		tabsize=p.size();
	}
	else{
		tabsize=p.size()-1;
	}
	for(unsigned int i =0;i<tabsize;i+=2){
		r=rand()%size;
		//cout<<i<<endl;
		char tmp[p[i].size]; 
		strcpy(tmp, p[i].genome);

		for(int j = r;j<size;++j){
					if(rand()%100<5){
						p[i].genome[j]=genRandom();
					}
					else{
						p[i].genome[j]=getGaussGene(p[i].genome[j],p[i+1].genome[j]);
					}
					if(rand()%100<5){
						p[i+1].genome[j]=genRandom();
					}
					else{
					p[i+1].genome[j]=getGaussGene(tmp[j],p[i+1].genome[j]);
					}
		}
		result.push_back(p[i]);
		result.push_back(p[i+1]);

	}
	if(p.size()%2!=0){
		int i=p.size()-1;
		for(int j = r;j<size;++j){
					if(rand()%100<5){
								p[i].genome[j]=genRandom();
							}
							else{
								p[i].genome[j]=p[i-1].genome[j];
							}
				}
		result.push_back(p[i]);
	}
	for(unsigned int i = 0;i<result.size();++i){
		result[i].fitness=numeric_limits<double>::infinity();
	}
	return result;
}


Populacja 	breed(Populacja p){
		
	
	p = normalize(p);
	//cout<<"normalizacja\n"<<endl;
	//printPopulacja(p);

	p = rulette(p);
	//cout<<"ruletka\n"<<endl;
	//printPopulacja(p);

	p = crossover(p);
	//cout<<"cross\n"<<endl;
	//printPopulacja(p);
	return p;
}


#endif /* OSOBNIK_H_ */
