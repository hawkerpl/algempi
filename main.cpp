/*
 * main.cpp
 *
 *  Created on: 2-14 maj 2014
 *      Authors: hawker and Marcin Obyrtacz
 */
#include "Osobnik.h"
#include <iostream>

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "mpi.h"


/** Ogolny zarys tego co tu sie dzieje
 * initializePopulation()
 * Poczatek petli
 *		dividePopulation()
 *			sendMasterToSlavePopulation()
 *			...
 *			...(Master wysy�a dane do slave)
 *			...
 *				recvOnSlavePopulation()
 *					countFitnessOnSlave()
 *					sendSlaveToMasterPopulation();
 *			...
 *			...(Master odbiera dane)
 *			...
 *			recvOnMasterPopulation()
 *			joinPopulation()
 *
 *		sortPopulation(pop);
 *		breed(pop);
 *Koniec Petli
 *
 *
 */




/**
 * Liczy dla kazdego osobnika wartosc funkcji dostosowania
 * @param p populacja
 * @param target cel, string jaki ma nam wyjsc
 * @return	populacja z wyliczona wartoscia funkcji dost
 */

Populacja 	countFitnessOnSlave(Populacja& p,string target){
	for(unsigned int i = 0; i<p.size(); ++i){
		p[i].fitness = fitness(p[i].genome, target, p[i].size);
	}	
	return p;
}



/**
 * ./a.out costam 1000 40
 * @param argc
 * @param argv
 * @return
 */

int main(int argc, char* argv[]){

	if(argc!= 4){
		cout<<"zla liczba argumentow"<<endl;
		return 0;
	}

	srand(time(NULL));

	//nazwa pliku z kt�rego wczytujemy tekst do wygenerowania
	char* filename = argv[1];

	/**
	 * wczytywanie pliku
	 */
	string line, target;
	stringstream ss1;
	fstream file;

	file.open(filename, ios::in);
	if(file.good() == true) {
		while(!file.eof()) {
			getline(file, line);
			ss1 << line;
		}
		file.close();
	}
	target = ss1.str();

	
	int len = target.size();

	//wczytujemy liczbe iteracji
	stringstream ss;
	ss << argv[2];
	int iterations = 0;
	ss >> iterations;

	//nasza populacja
	string tmps2(argv[3]);
	stringstream ss2(tmps2);
	int popSize;
	ss2 >> popSize;
	Populacja pop(popSize), pSlave;

	//Inicjalizacja MPI
	int myid, numprocs, slaveNum, master = 0;
	MPI_Status status1, status2, status3, status4, status5;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

	slaveNum = numprocs - 1;

	pop = initializePopulation(pop, len);


	for(int iter = 0; iter < iterations; ++iter) {	
		Podpopulacja podpop;
		podpop = dividePopulation(pop, slaveNum);
		
		//Rozeslanie podpopulacji do procesow slave
		if(myid == 0) {
			for(int i = 1; i <= slaveNum; ++i) {
				int popSize = podpop[i-1].size();
				MPI_Send(&popSize, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
				for(int j = 0; j < popSize; ++j) {
					MPI_Send(&podpop[i-1].at(j).size, 	   1, 	 MPI_INT, 	 i, 2, MPI_COMM_WORLD);					
					MPI_Send(podpop[i-1].at(j).genome, 	   len,  MPI_CHAR, 	 i, 3, MPI_COMM_WORLD);
					MPI_Send(&podpop[i-1].at(j).fitness,   1, 	 MPI_DOUBLE, i, 4, MPI_COMM_WORLD);
					MPI_Send(&podpop[i-1].at(j).evaulated, 1, 	 MPI_C_BOOL, i, 5, MPI_COMM_WORLD);
				}
			}
		} else {
			//Odebranie przez kazdy proces swojej podpopulacji
			int myPopSize;
			MPI_Recv(&myPopSize, 1, MPI_INT, master, 1, MPI_COMM_WORLD, &status1);
			pSlave.clear();
			pSlave.resize(myPopSize);
			for(int i = 0; i < myPopSize; ++i) {
				MPI_Recv(&pSlave[i].size,  		1, 	    MPI_INT,    master, 2, MPI_COMM_WORLD, &status2);				
				pSlave[i].genome = new char[len];
				MPI_Recv(pSlave[i].genome, 		len, 	MPI_CHAR,   master, 3, MPI_COMM_WORLD, &status3);								
				MPI_Recv(&pSlave[i].fitness, 	1,	    MPI_DOUBLE, master, 4, MPI_COMM_WORLD, &status4);
				MPI_Recv(&pSlave[i].evaulated, 	1, 	    MPI_CHAR,   master, 5, MPI_COMM_WORLD, &status5);
			}

			//Usuniecie smieci
			for(int i = 0; i < pSlave.size(); ++i) {
				char tmp[len];  
				if(strlen(pSlave[i].genome) > len) {
					strncpy(tmp, pSlave[i].genome, len);
					pSlave[i].size = len;
					strcpy(pSlave[i].genome, tmp);
				} else {
					strcpy(tmp, pSlave[i].genome);
				}
			}


			countFitnessOnSlave(pSlave, target);


			//Wysylanie od slave'wow do mastera
			myPopSize = pSlave.size();
			MPI_Send(&myPopSize, 1, MPI_INT, 0, myid*10+myid, MPI_COMM_WORLD);
			for(int i = 0; i < myPopSize; ++i) {
				MPI_Send(&pSlave[i].size, 	 	1, 	 	MPI_INT, 	 0, myid*20+myid, MPI_COMM_WORLD);					
				MPI_Send(pSlave[i].genome, 	 	len,    MPI_CHAR, 	 0, myid*30+myid, MPI_COMM_WORLD);
				MPI_Send(&pSlave[i].fitness, 	1, 	 	MPI_DOUBLE,  0, myid*40+myid, MPI_COMM_WORLD);
				MPI_Send(&pSlave[i].evaulated,  1, 	 	MPI_C_BOOL,  0, myid*50+myid, MPI_COMM_WORLD);
			}
			
		}


		//Odbierania przez mastera danych od slave'ow
		if(myid == 0) {
			pop.clear();
			for(int i = 1; i <= slaveNum; ++i) {
				int podpopSize;
				MPI_Recv(&podpopSize, 1, MPI_INT, i, i*10+i, MPI_COMM_WORLD, &status1);
				pop.resize(pop.size() + podpopSize);				
				for(int j = pop.size()-podpopSize; j < pop.size(); ++j) {
					MPI_Recv(&pop[j].size, 	 	 1, 	 MPI_INT, 	 i, i*20+i, MPI_COMM_WORLD, &status2);					
					int genLen = pop[j].size;				
					pop[j].genome = new char[genLen];
					MPI_Recv(pop[j].genome, 	 genLen, MPI_CHAR, 	 i, i*30+i, MPI_COMM_WORLD, &status3);
					MPI_Recv(&pop[j].fitness, 	 1, 	 MPI_DOUBLE, i, i*40+i, MPI_COMM_WORLD, &status4);
					MPI_Recv(&pop[j].evaulated,  1, 	 MPI_C_BOOL, i, i*50+i, MPI_COMM_WORLD, &status5);
				}
			}
		}

		//Usuniecie smieci
		for(int i = 0; i < pop.size(); ++i) {
			char tmp[len];  
			if(strlen(pop[i].genome) > len) {
				strncpy(tmp, pop[i].genome, len);
				pop[i].size = len;
				strcpy(pop[i].genome, tmp);
			} else {
				strcpy(tmp, pop[i].genome);
			}
		}
		

		
		//Sortowanie i rozmnazanie populacji
		if(myid == 0) {
			pop = sortPopulation(pop);
			pop = breed(pop);	
		}

	}

	if (myid==0) {
		pop=sortPopulation(pop);
		printPopulacja(pop);
    }


	MPI_Finalize();
	return 0;
}
